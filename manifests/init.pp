class modulo_marivalle{
class {'apache':}

apache::vhost { 'myMpwar.prod':
	port	=> '80',
	docroot	=> '/var/www/prod/',
	docroot_owner	=> 'vagrant',
	docroot_group => 'vagrant',
}

apache::vhost { 'myMpwar.dev':
	port	=> '80',
	docroot	=> '/var/www/dev/',
	docroot_owner	=> 'vagrant',
	docroot_group => 'vagrant',
}

file { "/var/www/prod/":
    ensure => "directory",
}
file { '/var/www/prod/index.php':
    ensure => file,
    replace => true,
    content  => "Hello World. Sistema operativo ${operatingsystem} ${operatingsystemrelease}",
    require => File ["/var/www/prod/"]
}

file { "/var/www/dev/":
    ensure => "directory",
}
file { '/var/www/dev/index.php':
    ensure => file,
    replace => true,
    content  => "<?php phpinfo();",
    require => File ["/var/www/dev/"]
}

$php_version = '55'

if $php_version == '55' {
  $yum_repo = 'remi-php55'
  include ::yum::repo::remi_php55
}

class { 'php':
  version => 'latest',
  require => Yumrepo[$yum_repo]
}

php::module { [ 'devel', 'pear', 'mbstring', 'xml', 'tidy', 'pecl-memcache', 'soap' ]: }

include memcached

class { '::ntp':
  servers => [ '1.es.pool.ntp.org', '2.europe.pool.ntp.org', '3.europe.pool.ntp.org' ],
  package_ensure => 'latest'
}

class {'mysql':}

mysql::grant{ 'mpwar_test':
        mysql_privileges => 'ALL',
        mysql_db => 'mpwar_test',
        mysql_user => 'mpwar_test',
        mysql_password => 'pwd',
        mysql_host => '127.0.0.1'
}


mysql::grant{ 'mympwar':
        mysql_privileges => 'ALL',
        mysql_db => 'mympwar',
        mysql_user => 'mympwar',
        mysql_password => 'pwd',
        mysql_host => '127.0.0.1'
}

}
